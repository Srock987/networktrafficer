SELECT TIME(MAX(continuance)) as time_spend, stamp_inserted as started_watching
FROM (SELECT acct.*,
@a:=TIMEDIFF(stamp_inserted, @d) AS timedifference,
@m:= if(time_to_sec(TIMEDIFF(stamp_inserted, @d))<360,TRUE,FALSE) AS minus,
@f:=CONVERT(
 IF( @b<=>ip_dst AND @c<=>ip_src AND @m , @f, stamp_inserted), DATETIME
) AS Begin,
@b:=ip_dst AS DESTINATION,
@c:=ip_src AS SOURCE,
@d:=stamp_inserted AS TIMESTAMP,
@continuance:=IF(@m,TIMEDIFF(@d, @f),0) AS continuance
FROM acct JOIN (SELECT @c:=NULL ) AS init
LEFT JOIN youtube_address AS youtube
ON ip_src LIKE ip_address
WHERE ip_dst=:certainIp AND ip_src=:youtubeIp
ORDER BY stamp_inserted
) AS t GROUP BY t.Begin, stamp_inserted