package com.odis.networktrafficer.controller;

import com.odis.networktrafficer.model.Acct;
import com.odis.networktrafficer.model.Activity;
import com.odis.networktrafficer.model.Traffic;
import com.odis.networktrafficer.model.YoutubeTimer;
import com.odis.networktrafficer.repository.AcctRepository;
import com.odis.networktrafficer.setup.ShellExecutor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("acct")
public class AcctController {

    @Autowired
    AcctRepository acctRepository;

    /*
    Method returns all Acct data stored in database
    and parses it in form of JSON
    */

    @RequestMapping(value = "/getAllAcct",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Acct> getAllAcct(){
        return acctRepository.getAllAcct();
    }

    /*
    Method returns Traffic data on single ip address
    based on Acct data stored in database
    and parses it in form of JSON
    */

    @RequestMapping(value = "/getIpTraffic/{ip}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Traffic> getIpTraffic(@PathVariable(name = "ip") String ipAddress){
        return acctRepository.getUserServersTraffic(ipAddress);
    }

    /*
    Method returns Packets data on single ip address
    specified in the URL as path variable
    for two weeks time period ending on current date
    based on Acct data stored in database
    and parses it in form of JSON
    */

    @RequestMapping(value = "/getIpTwoWeeksPackets/{ip}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Activity> getIpTwoWeeksPacktes(@PathVariable(name = "ip") String ipAddress){
        return acctRepository.getIpTwoWeeksActivity(ipAddress);
    }

    /*
    Method returns Packets data on single ip address
    specified in the URL as path variable
    for single date specified in the URL as path variable
    based on Acct data stored in database
    and parses it in form of JSON
    */

    @RequestMapping(value = "/getIpDayPackets/{ip}/{date}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Activity> getIpDayPacktes(@PathVariable(name = "ip") String ipAddress, @PathVariable(name = "date") String date){
        return acctRepository.getIpDayActivity(ipAddress,date);
    }

    /*
    Method returns Packets data on single ip address
    specified in the URL as path variable
    for hour of a day specified in the URL as path variable
    based on Acct data stored in database
    and parses it in form of JSON
    */

    @RequestMapping(value = "/getIpHourPackets/{ip}/{datetime}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<Activity> getIpHourPacktes(@PathVariable(name = "ip") String ipAddress, @PathVariable(name = "datetime") String datetime){
        return acctRepository.getIpHourActivity(ipAddress,datetime);
    }

    /*
    Method returns YoutubeTimer data on single ip address
    specified in the URL as path variable
    based on Acct data stored in database
    and parses it in form of JSON
    */

    @RequestMapping(value = "/getIpYoutubeAcct/{ip}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public List<YoutubeTimer> getIpYoutubeAcct(@PathVariable(name = "ip") String ipAddress){
        return acctRepository.getUserYoutubeAcct(ipAddress);
    }

    /*
    Method install Pmacctd package needed
    for gathering of network transfer information
    Returns OK on successful run and FAIL otherwise
    */

    @RequestMapping(value = "/install",
            method = RequestMethod.GET)
    public String install(){
        String response = "FAIL";
        try {
            if(ShellExecutor.executeCommands(ShellExecutor.ShellCommand.INSTALL)){
                response = "OK";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
    Method starts Pmacctd package needed
    for gathering of network transfer information
    Returns OK on successful run and FAIL otherwise
    */

    @RequestMapping(value = "/run",
            method = RequestMethod.GET)
    public String run(){
        String response = "FAIL";
        try {
            if(ShellExecutor.executeCommands(ShellExecutor.ShellCommand.RUN)){
                response = "OK";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }

    /*
    Method cleans all gathered Acct data in database
    */

    @RequestMapping(value = "/clean",
            method = RequestMethod.GET)
    public String clean(){
        String response = "OK";
        acctRepository.cleanDatabase();
        return response;
    }

    /*
    Method dumps to file all gathered Acct data in database
    */

    @RequestMapping(value = "/dump",
            method = RequestMethod.GET)
    public String dump(){
        String response = "OK";
        try {
            ShellExecutor.executeCommands(ShellExecutor.ShellCommand.DUMP);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return response;
    }


}
