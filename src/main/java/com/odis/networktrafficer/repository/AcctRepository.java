package com.odis.networktrafficer.repository;

import com.odis.networktrafficer.mapper.*;
import com.odis.networktrafficer.model.Acct;
import com.odis.networktrafficer.model.Activity;
import com.odis.networktrafficer.model.Traffic;
import com.odis.networktrafficer.model.YoutubeTimer;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/*
Class in representation of the repository of Acct
object stored in the acct database. It contains all the
queries performed on the database.
*/

@Repository
public class AcctRepository {
    private static final String GET_ALL_ACCT = "SELECT * FROM acct";
    private static final String GET_IP_YOUTUBE_ACCT = "SELECT TIME(MAX(continuance)) AS time_wasted,t.Begin AS started_watching FROM (SELECT acct.*, @a:=TIMEDIFF(stamp_inserted, @d) AS timedifference, @m:= if(time_to_sec(TIMEDIFF(stamp_inserted, @d))<360,TRUE,FALSE) AS minus, @f:=CONVERT( IF( @b<=>ip_dst AND @c<=>ip_src AND @m , @f, stamp_inserted), DATETIME ) AS Begin, @b:=ip_dst AS DESTINATION, @c:=ip_src AS SOURCE, @d:=stamp_inserted AS TIMESTAMP, @continuance:=IF(@m,TIMEDIFF(@d, @f),0) AS continuance FROM acct JOIN (SELECT @c:=NULL ) AS init INNER JOIN youtube_address AS youtube ON LEFT(acct.ip_src,LENGTH(acct.ip_src)-LOCATE('.',REVERSE(acct.ip_src))) = LEFT(youtube.ip_address,LENGTH(youtube.ip_address)-LOCATE('.',REVERSE(youtube.ip_address))) ORDER BY stamp_inserted ) AS t GROUP BY t.Begin";
    private static final String GET_IN_TRAFFIC_IP = "SELECT ip_src, SUM(bytes) FROM acct WHERE ip_dst=:certainIp GROUP BY ip_src";
    private static final String GET_OUT_TRAFFIC_IP = "SELECT ip_dst, SUM(bytes) FROM acct WHERE ip_src=:certainIp GROUP BY ip_dst";
    private static final String GET_TWO_WEEKS_ACTIVITY_IP = "SELECT ip_dst, SUM(packets) as packets, TIMESTAMP(DATE(stamp_inserted)) as stamp_inserted FROM acct WHERE ip_src=:certainIp AND stamp_inserted BETWEEN DATE_SUB(CURRENT_DATE(), INTERVAL 2 WEEK) AND CURRENT_DATE() GROUP BY ip_dst, TIMESTAMP(DATE(stamp_inserted))";
    private static final String GET_DAY_ACTIVITY_IP = "SELECT ip_dst, SUM(packets) as packets, TIMESTAMPADD(HOUR,HOUR(stamp_inserted) ,DATE(stamp_inserted)) as stamp_inserted FROM acct WHERE ip_src=:certainIp AND DATE(stamp_inserted) = DATE(:date) GROUP BY TIMESTAMPADD(HOUR,HOUR(stamp_inserted),DATE(stamp_inserted)), ip_dst";
    private static final String GET_HOUR_ACTIVITY_IP = "SELECT ip_dst, packets, stamp_inserted FROM acct WHERE ip_src=:certainIp AND stamp_inserted BETWEEN TIMESTAMP(:datetime) AND DATE_ADD(TIMESTAMP(:datetime), INTERVAL 1 HOUR)";
    private static final String DELETE_TABLE_ACCT = "DELETE FROM acct";
    private static final String GET_ACCT_COUNT = "SELECT COUNT(*) as total FROM acct";
    private static final String INSERT_ACCT = "INSERT INTO acct VALUES (:macSrc,:macDst,:ipSrc,:ipDst,:srcPort,:dstPort,:ipProto,:packets,:bytes,:stampInserted,:stampUpdated)";
    private final NamedParameterJdbcTemplate namedTemplate;

    public AcctRepository(@Qualifier("dataSource") DataSource dataSource) {
        this.namedTemplate = new NamedParameterJdbcTemplate(dataSource);
    }

    /*
    Method queries for all the Acct date stored in database
    */

    public List<Acct> getAllAcct(){
        return namedTemplate.query(GET_ALL_ACCT, new BeanPropertyRowMapper<Acct>(Acct.class));
    }

    /*
    Method inserts the Acct object to the database.
    This in not used during application execution
    but only for testing purposes.
    */

    public void insertAcct(Acct acct){
        Map<String, Object> parameters = new HashMap<String, Object>();
        parameters.put("macSrc",acct.getMacSrc());
        parameters.put("macDst",acct.getMacDst());
        parameters.put("ipSrc",acct.getIpSrc());
        parameters.put("ipDst",acct.getIpDst());
        parameters.put("srcPort",acct.getSrcPort());
        parameters.put("dstPort",acct.getDstPort());
        parameters.put("ipProto",acct.getIpProto());
        parameters.put("packets",acct.getPackets());
        parameters.put("bytes",acct.getBytes());
        parameters.put("stampInserted",acct.getStampInserted());
        parameters.put("stampUpdated",acct.getStampUpdated());
        namedTemplate.update(INSERT_ACCT,parameters);
    }

    /*
    Method queries database for all the Traffic
    going in and out of certain IP address.
    */

    public List<Traffic> getUserServersTraffic(String ipAddress){
        Map<String,String> parameterMap = new HashMap<>();
        parameterMap.put("certainIp",ipAddress);
        List<Traffic> trafficList = namedTemplate.query(GET_IN_TRAFFIC_IP, parameterMap, new TrafficInMapper());
        trafficList.addAll(namedTemplate.query(GET_OUT_TRAFFIC_IP, parameterMap, new TrafficOutMapper()));
        return trafficList;
    }

    /*
    Method queries database for all Activity of one IP address
    within two weeks period ending on current date.
    */

    public List<Activity> getIpTwoWeeksActivity(String ipAddress){
        Map<String,String> parameterMap = new HashMap<>();
        parameterMap.put("certainIp",ipAddress);
        return  namedTemplate.query(GET_TWO_WEEKS_ACTIVITY_IP, parameterMap, new ActivityMapper());
    }

    /*
    Method queries database for all Activity of one IP address
    within period of one day stated by date parameter.
    */

    public List<Activity> getIpDayActivity(String ipAddress, String date){
        Map<String,String> parameterMap = new HashMap<>();
        parameterMap.put("certainIp",ipAddress);
        parameterMap.put("date",date);
        return  namedTemplate.query(GET_DAY_ACTIVITY_IP, parameterMap, new ActivityMapper());
    }

    /*
    Method queries database for all Activity of one IP address
    within period of one hour stated by datetime parameter.
    */

    public List<Activity> getIpHourActivity(String ipAddress, String datetime){
        Map<String,String> parameterMap = new HashMap<>();
        parameterMap.put("certainIp",ipAddress);
        parameterMap.put("datetime",datetime);
        return  namedTemplate.query(GET_HOUR_ACTIVITY_IP, parameterMap, new ActivityMapper());
    }

    /*
    Method queries database for all YoutubeTimer of one IP address.
    */

    public List<YoutubeTimer> getUserYoutubeAcct(String ipAddress){
        Map<String,String> parameterMap = new HashMap<>();
        parameterMap.put("certainIp",ipAddress);
        namedTemplate.query(GET_IP_YOUTUBE_ACCT, parameterMap, new YoutubeMapper());
        return namedTemplate.query(GET_IP_YOUTUBE_ACCT, parameterMap, new YoutubeMapper());
    }

    /*
    Method cleans database from all the Acct records
    */

    public void cleanDatabase(){
        Map<String,String> parameterMap = new HashMap<>();
        namedTemplate.update(DELETE_TABLE_ACCT,parameterMap);
    }

    /*
    Method queries the database for count of all the Acct records
    */

    public Integer getAcctCount(){
        Map<String,String> parameterMap = new HashMap<>();
        return namedTemplate.queryForObject(GET_ACCT_COUNT, parameterMap, Integer.class);
    }
}
