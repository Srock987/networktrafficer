package com.odis.networktrafficer.model;

/*
This class in POJO for the Acct object stored in database.
It contains all data gathered from Pmacctd package.
*/

public class Acct {

  private String macSrc;
  private String macDst;
  private String ipSrc;
  private String ipDst;
  private long srcPort;
  private long dstPort;
  private String ipProto;
  private long packets;
  private long bytes;
  private java.sql.Timestamp stampInserted;
  private java.sql.Timestamp stampUpdated;


  public String getMacSrc() {
    return macSrc;
  }

  public void setMacSrc(String macSrc) {
    this.macSrc = macSrc;
  }


  public String getMacDst() {
    return macDst;
  }

  public void setMacDst(String macDst) {
    this.macDst = macDst;
  }


  public String getIpSrc() {
    return ipSrc;
  }

  public void setIpSrc(String ipSrc) {
    this.ipSrc = ipSrc;
  }


  public String getIpDst() {
    return ipDst;
  }

  public void setIpDst(String ipDst) {
    this.ipDst = ipDst;
  }


  public long getSrcPort() {
    return srcPort;
  }

  public void setSrcPort(long srcPort) {
    this.srcPort = srcPort;
  }


  public long getDstPort() {
    return dstPort;
  }

  public void setDstPort(long dstPort) {
    this.dstPort = dstPort;
  }


  public String getIpProto() {
    return ipProto;
  }

  public void setIpProto(String ipProto) {
    this.ipProto = ipProto;
  }


  public long getPackets() {
    return packets;
  }

  public void setPackets(long packets) {
    this.packets = packets;
  }


  public long getBytes() {
    return bytes;
  }

  public void setBytes(long bytes) {
    this.bytes = bytes;
  }


  public java.sql.Timestamp getStampInserted() {
    return stampInserted;
  }

  public void setStampInserted(java.sql.Timestamp stampInserted) {
    this.stampInserted = stampInserted;
  }


  public java.sql.Timestamp getStampUpdated() {
    return stampUpdated;
  }

  public void setStampUpdated(java.sql.Timestamp stampUpdated) {
    this.stampUpdated = stampUpdated;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj){
      return true;
    }
    if (obj == null){
      return false;
    }
    if (getClass() != obj.getClass()){
      return  false;
    }
    Acct o = (Acct) obj;
    boolean value = true;
    if (!macSrc.equals(o.macSrc)){
      value = false;
    }
    if (!macDst.equals(o.macDst)){
      value = false;
    }
    if (!ipSrc.equals(o.ipSrc)){
      value = false;
    }
    if (srcPort != o.srcPort){
      value = false;
    }
    if (dstPort != o.dstPort){
      value = false;
    }
    if (!ipProto.equals(o.ipProto)){
      value = false;
    }
    if (packets != o.packets){
      value = false;
    }
    if (bytes != o.bytes){
      value = false;
    }
    if (!stampInserted.equals(o.stampInserted)){
      value = false;
    }
    if (!stampUpdated.equals(o.stampUpdated)){
      value = false;
    }
    return value;
  }

  @Override
  public String toString() {
    return "Acct{" +
            "macSrc='" + macSrc + '\'' +
            ", macDst='" + macDst + '\'' +
            ", ipSrc='" + ipSrc + '\'' +
            ", ipDst='" + ipDst + '\'' +
            ", srcPort=" + srcPort +
            ", dstPort=" + dstPort +
            ", ipProto='" + ipProto + '\'' +
            ", packets=" + packets +
            ", bytes=" + bytes +
            ", stampInserted=" + stampInserted +
            ", stampUpdated=" + stampUpdated +
            '}';
  }
}
