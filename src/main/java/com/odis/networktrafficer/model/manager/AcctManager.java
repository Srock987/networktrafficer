package com.odis.networktrafficer.model.manager;

import com.odis.networktrafficer.model.Acct;

import java.sql.Timestamp;
import java.util.Random;

public class AcctManager {

    /*
    Method creates default Acct object.
    This was created mainly for testing purposes.
    */

    static Random rand = new Random();

    public static Acct createDefaultAcct(String ipSrc, String ipDst, Timestamp inserted, Timestamp updated){
        Acct acct = new Acct();
        acct.setMacSrc("0.0.0.0");
        acct.setMacDst("0.0.0.0");
        acct.setIpSrc(ipSrc);
        acct.setIpDst(ipDst);
        acct.setSrcPort(2222);
        acct.setDstPort(3333);
        acct.setIpProto("proto");
        acct.setPackets(100+rand.nextInt(100));
        acct.setBytes(400+rand.nextInt(200));
        inserted.setNanos(0);
        acct.setStampInserted(inserted);
        updated.setNanos(0);
        acct.setStampUpdated(updated);
        return acct;
    }

}
