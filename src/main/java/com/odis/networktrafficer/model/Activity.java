package com.odis.networktrafficer.model;

import java.sql.Timestamp;

/*
This class in POJO for the Activity.
It isn't stored in database but created on querying and ActivityMapper.
It represents package transfer from the server of certain ip
and is marked by timestamp
*/

public class Activity {
    private String serwer;
    private long packets;
    private java.sql.Timestamp timestamp;

    public Activity() {
    }

    public Activity(String serwer, long packets, Timestamp timestamp) {
        this.serwer = serwer;
        this.packets = packets;
        this.timestamp = timestamp;
    }

    public String getSerwer() {
        return serwer;
    }

    public void setSerwer(String serwer) {
        this.serwer = serwer;
    }

    public long getPackets() {
        return packets;
    }

    public void setPackets(long packets) {
        this.packets = packets;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Timestamp timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Activity{" +
                "serwer='" + serwer + '\'' +
                ", packets=" + packets +
                ", timestamp=" + timestamp +
                '}';
    }
}
