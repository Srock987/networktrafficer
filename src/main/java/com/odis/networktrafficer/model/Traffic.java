package com.odis.networktrafficer.model;

/*
This class in POJO for the Traffic.
It isn't stored in database but created on querying and TrafficMappers.
It represents byte transfer from the server of certain ip
*/

public class Traffic {
    private String server;
    private long bytes;

    public Traffic() {
    }

    public Traffic(String server, long bytes) {
        this.server = server;
        this.bytes = bytes;
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public long getBytes() {
        return bytes;
    }

    public void setBytes(long bytes) {
        this.bytes = bytes;
    }
}
