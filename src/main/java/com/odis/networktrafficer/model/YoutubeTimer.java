package com.odis.networktrafficer.model;

import java.sql.Time;
import java.sql.Timestamp;

/*
This class in POJO for the YoutubeTimer.
It isn't stored in database but created on querying and YoutubeMapper.
It represents continuous watching of Youtube content and contains
information about time which the connection was established and
how long did it last. Continuous watching is meant as connection where
packets were transferred in between 5 minutes intervals.
*/

public class YoutubeTimer {
    private java.sql.Time timeSpent;
    private java.sql.Timestamp startedWatching;

    public YoutubeTimer() {
    }

    public YoutubeTimer(Time timeSpent, Timestamp startedWatching) {
        this.timeSpent = timeSpent;
        this.startedWatching = startedWatching;
    }

    public Time getTimeSpent() {
        return timeSpent;
    }

    public void setTimeSpent(Time timeSpent) {
        this.timeSpent = timeSpent;
    }

    public Timestamp getStartedWatching() {
        return startedWatching;
    }

    public void setStartedWatching(Timestamp startedWatching) {
        this.startedWatching = startedWatching;
    }
}
