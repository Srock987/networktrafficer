package com.odis.networktrafficer.setup;

import java.io.*;

/*
This class contains allows for the program to
create and execute bash scripts in the console.
For most commands the application needs to be started
withing sudo rights
*/

public class ShellExecutor {

    /*
    Enum listing all the supported types of shell commands
    */

    public enum ShellCommand{
        INSTALL,
        RUN,
        DUMP
    }

    /*
    This method execute created command and deletes the script afterwards
    */

    public static boolean executeCommands(ShellCommand command) throws IOException {
        File tempScript = createTempScript(command);
        try {
            ProcessBuilder pb = new ProcessBuilder("bash", tempScript.toString());
            pb.inheritIO();
            Process process = pb.start();
            process.waitFor();
            return true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        } finally {
            if(!tempScript.delete()){
                System.out.println("Couldn't delete file");
            }
        }
        return false;
    }

    /*
    This method create temporary script command based on the
    ShellCommand input type
    */

    private static File createTempScript(ShellCommand command) throws IOException {
        File tempScript = File.createTempFile("script", null);

        Writer streamWriter = new OutputStreamWriter(new FileOutputStream(
                tempScript));
        PrintWriter printWriter = new PrintWriter(streamWriter);

        switch (command){
            case INSTALL:
                printWriter = installScript(printWriter);
                break;
            case RUN:
                printWriter = runPmacctd(printWriter);
                break;
            case DUMP:
                printWriter = dumpDatabase(printWriter);
                break;
            default:
                printWriter.println("#!/bin/bash");
                printWriter.println("echo something went wrong");
                break;
        }

        printWriter.close();

        return tempScript;
    }

    /*
    This method creates the script in order to install
    the Pmacct package on the local machine
    */

    private static PrintWriter installScript(PrintWriter printWriter){
        printWriter.println("#!/bin/bash");
        printWriter.println("echo INSTALL");
        printWriter.println("sudo apt-get update && sudo apt-get upgrade -y");
        printWriter.println("sudo apt-get install mysql-server mysql-client -y");
        printWriter.println("sudo apt-get install curl libpcap-dev libmysqlclient-dev -y");
        printWriter.println("curl -O http://www.pmacct.net/pmacct-1.7.0.tar.gz");
        printWriter.println("tar -zxvf pmacct-1.7.0.tar.gz");
        printWriter.println("cd pmacct-1.7.0/");
        printWriter.println("./configure --enable-mysql");
        printWriter.println("make");
        printWriter.println("sudo make install");
        printWriter = setupDatabase(printWriter);
        return printWriter;
    }

    /*
    This method creates the script in order to start Pmacctd
    package for gathering the network communication data from
    configuration stored in my_pmacctd.conf file
    */

    private static PrintWriter runPmacctd(PrintWriter printWriter){
        printWriter.println("#!/bin/bash");
        printWriter.println("echo RUN");
        printWriter.println("sudo pmacctd -f my_pmacctd.conf");
        return printWriter;
    }

    /*
    This method creates the script in order to
    configure mysql database for the Pmacct package to run
    */

    private static PrintWriter setupDatabase(PrintWriter printWriter){
        printWriter.println("cd sql/");
        printWriter.println("sudo mysql -u root -p < pmacct-create-db_v1.mysql");
        printWriter.println("sudo mysql -u root -p < pmacct-grant-db.mysql");
        return printWriter;
    }

    /*
    This method creates new file and dumps data from current database to the file
    */

    private static PrintWriter dumpDatabase(PrintWriter printWriter){
        printWriter.println("#!/bin/bash");
        printWriter.println("mysql -u pmacct -parealsmartpwd pmacct > pmacct_backup_"+String.valueOf(System.currentTimeMillis())+".sql");
        return printWriter;
    }
}
