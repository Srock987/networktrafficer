package com.odis.networktrafficer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
public class NetworkTrafficerApplication {

	@PostConstruct
	void started() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC+02:00"));
	}

	public static void main(String[] args) {
		SpringApplication.run(NetworkTrafficerApplication.class, args);
	}
}
