package com.odis.networktrafficer.mapper;

import com.odis.networktrafficer.model.Activity;
import org.springframework.jdbc.core.RowMapper;

import javax.validation.constraints.NotNull;
import java.sql.ResultSet;
import java.sql.SQLException;


public class ActivityMapper implements RowMapper<Activity> {

    /*
    Method maps the response for Activity query
    the the Activity POJO
    */

    @Override
    public Activity mapRow(ResultSet resultSet, int i) throws SQLException {
        Activity activity = new Activity();
        activity.setSerwer(resultSet.getString("ip_dst"));
        activity.setPackets(resultSet.getLong("packets"));
        activity.setTimestamp(resultSet.getTimestamp("stamp_inserted"));
        return activity;
    }
}
