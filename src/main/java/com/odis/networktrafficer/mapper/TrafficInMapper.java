package com.odis.networktrafficer.mapper;

import com.odis.networktrafficer.model.Traffic;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class TrafficInMapper implements RowMapper<Traffic> {

    /*
    Method maps the response for Traffic query of incoming packages
    the the Traffic POJO
    */

    @Override
    public Traffic mapRow(ResultSet resultSet, int i) throws SQLException {
        Traffic traffic = new Traffic();
        traffic.setServer(resultSet.getString("ip_src"));
        traffic.setBytes(resultSet.getLong("Sum(bytes)"));
        return traffic;
    }
}
