package com.odis.networktrafficer.mapper;

import com.odis.networktrafficer.model.YoutubeTimer;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.lang.Nullable;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;

public class YoutubeMapper implements RowMapper<YoutubeTimer> {

    /*
    Method maps the response for YoutubeTimer query
    the the YoutubeTimer POJO
    */

    @Nullable
    @Override
    public YoutubeTimer mapRow(ResultSet resultSet, int i) throws SQLException {
        YoutubeTimer youtubeTimer = new YoutubeTimer();
        Time time = resultSet.getTime("time_wasted");
        youtubeTimer.setTimeSpent(time);
        Timestamp timestamp = resultSet.getTimestamp("started_watching");
        youtubeTimer.setStartedWatching(timestamp);
        return youtubeTimer;
    }
}
