package com.odis.networktrafficer;

import com.odis.networktrafficer.model.Acct;
import com.odis.networktrafficer.model.Activity;
import com.odis.networktrafficer.model.Traffic;
import com.odis.networktrafficer.model.YoutubeTimer;
import com.odis.networktrafficer.model.manager.AcctManager;
import com.odis.networktrafficer.repository.AcctRepository;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class NetworkTrafficerApplicationTests {

	@Autowired
	AcctRepository acctRepository;


	@Before
	public void clear(){
		System.out.println("CLEANED");
		acctRepository.cleanDatabase();
		assert (0 == acctRepository.getAcctCount());
	}

	@Test
	public void addSingleAcct(){
		Acct testAcct = AcctManager.createDefaultAcct("1.1.1.1","2.2.2.2",
				new Timestamp(System.currentTimeMillis()),
				new Timestamp(System.currentTimeMillis()));
		acctRepository.insertAcct(testAcct);
		List<Acct> acctList = acctRepository.getAllAcct();
		assert (acctList.size() == 1);
		assert (acctList.get(0).equals(testAcct));
	}

	@Test
	public void ipTraffic(){
		String userIp = "1.1.1.1";
		String server1 = "2.2.2.2";
		String server2 = "3.3.3.3";
		Acct testAcct = AcctManager.createDefaultAcct(userIp,server1,
				new Timestamp(System.currentTimeMillis()),
				new Timestamp(System.currentTimeMillis()));
		Acct testAcct1 = AcctManager.createDefaultAcct(userIp,server2,
				new Timestamp(System.currentTimeMillis()+1000),
				new Timestamp(System.currentTimeMillis()+1000));
		acctRepository.insertAcct(testAcct);
		acctRepository.insertAcct(testAcct1);
		List<Traffic> trafficList = acctRepository.getUserServersTraffic(userIp);
		assert (trafficList.size() == 2);
		for (Traffic traffic : trafficList) {
			if (traffic.getServer().equals(server1)){
				assert (traffic.getBytes() == testAcct.getBytes());
			} else if (traffic.getServer().equals(server2)){
				assert (traffic.getBytes() == testAcct1.getBytes());
			} else {
				assert (false);
			}
		}
	}

	@Test
	public void twoWeeksPackets(){
		String userIp = "1.1.1.1";
		String server1 = "2.2.2.2";
		int matchCount = 0;
		int millisInDay = 86400000;
		int dayCount = 14;
		List<Acct> acctList = new LinkedList<>();
		for (int i = 0; i<dayCount;i++){
			Acct testAcct = AcctManager.createDefaultAcct(userIp,server1,
					new Timestamp(System.currentTimeMillis()-i*millisInDay),
					new Timestamp(System.currentTimeMillis()-i*millisInDay));
			acctList.add(testAcct);
			acctRepository.insertAcct(testAcct);
		}
		List<Activity> activityList = acctRepository.getIpTwoWeeksActivity(userIp);

		for (Acct acct : acctList) {
			Date acctDate = new Date(acct.getStampInserted().getTime());
			Calendar acctCal = Calendar.getInstance();
			acctCal.setTime(acctDate);
			for (Activity activity : activityList) {
				Date activityDate = new Date(activity.getTimestamp().getTime());
				Calendar activityCal = Calendar.getInstance();
				activityCal.setTime(activityDate);
				boolean sameDay = acctCal.get(Calendar.YEAR) == activityCal.get(Calendar.YEAR) &&
								  acctCal.get(Calendar.DAY_OF_YEAR) == activityCal.get(Calendar.DAY_OF_YEAR);
				if (sameDay){
					System.out.println("FOUND");
					System.out.println(acctDate.toString());
					System.out.println(activityDate.toString());
					assert (activity.getPackets() == acct.getPackets() && activity.getSerwer().equals(server1));
					matchCount++;
				}
			}
		}
		System.out.println(matchCount);
	}

	@Test
    public void dayPackets(){
        String userIp = "1.1.1.1";
        String server1 = "2.2.2.2";
        int matchCount = 0;
        int millisInHour = 3600000;
        int hoursCount = 24;
        Calendar currentDayCal = Calendar.getInstance();
        currentDayCal.set(Calendar.DAY_OF_YEAR, currentDayCal.get(Calendar.DAY_OF_YEAR)-1);
        currentDayCal.set(Calendar.HOUR_OF_DAY,0);
        currentDayCal.set(Calendar.MINUTE,0);
        currentDayCal.set(Calendar.SECOND,0);
        currentDayCal.set(Calendar.MILLISECOND,0);
        List<Acct> acctList = new LinkedList<>();
        for (int i = 0; i<hoursCount;i++){
            Acct testAcct = AcctManager.createDefaultAcct(userIp,server1,
                    new Timestamp(currentDayCal.getTime().getTime()+i*millisInHour),
                    new Timestamp(currentDayCal.getTime().getTime()+i*millisInHour));
            acctList.add(testAcct);
            acctRepository.insertAcct(testAcct);
        }
        String currentTimeString = new Timestamp(currentDayCal.getTime().getTime()).toString();
        List<Activity> activityList = acctRepository.getIpDayActivity(userIp, currentTimeString);
        for (Acct acct : acctList) {
            Date acctDate = new Date(acct.getStampInserted().getTime());
            Calendar acctCal = Calendar.getInstance();
            acctCal.setTime(acctDate);
            for(Activity activity : activityList){
                Date activityDate = new Date(activity.getTimestamp().getTime());
                Calendar activityCal = Calendar.getInstance();
                activityCal.setTime(activityDate);
                boolean sameHour = acctCal.get(Calendar.YEAR) == activityCal.get(Calendar.YEAR) &&
                        acctCal.get(Calendar.DAY_OF_YEAR) == activityCal.get(Calendar.DAY_OF_YEAR) &&
                        acctCal.get(Calendar.HOUR_OF_DAY) == activityCal.get(Calendar.HOUR_OF_DAY);
                if (sameHour){
                    System.out.println("FOUND");
                    assert (activity.getPackets() == acct.getPackets() && activity.getSerwer().equals(server1));
                    matchCount++;
                }
            }
        }
        assert (matchCount == hoursCount);
    }

    @Test
    public void hourPackets(){
        String userIp = "1.1.1.1";
        String server1 = "2.2.2.2";
        int matchCount = 0;
        int millisInMinute = 60000;
        int minuteCount = 60;
        Calendar currentDayCal = Calendar.getInstance();
        currentDayCal.set(Calendar.HOUR_OF_DAY,currentDayCal.get(Calendar.HOUR_OF_DAY)-1);
        currentDayCal.set(Calendar.MINUTE,0);
        currentDayCal.set(Calendar.SECOND,0);
        currentDayCal.set(Calendar.MILLISECOND,0);
        List<Acct> acctList = new LinkedList<>();
        for (int i = 0; i<minuteCount;i++){
            Acct testAcct = AcctManager.createDefaultAcct(userIp,server1,
                    new Timestamp(currentDayCal.getTime().getTime()+i*millisInMinute),
                    new Timestamp(currentDayCal.getTime().getTime()+i*millisInMinute));
            acctList.add(testAcct);
            acctRepository.insertAcct(testAcct);
        }
        String currentTimeString = new Timestamp(currentDayCal.getTime().getTime()).toString();
        List<Activity> activityList = acctRepository.getIpHourActivity(userIp, currentTimeString);
        for (Acct acct : acctList) {
            Date acctDate = new Date(acct.getStampInserted().getTime());
            Calendar acctCal = Calendar.getInstance();
            acctCal.setTime(acctDate);
            for(Activity activity : activityList){
                Date activityDate = new Date(activity.getTimestamp().getTime());
                Calendar activityCal = Calendar.getInstance();
                activityCal.setTime(activityDate);
                boolean sameHour = acctCal.get(Calendar.YEAR) == activityCal.get(Calendar.YEAR) &&
                        acctCal.get(Calendar.DAY_OF_YEAR) == activityCal.get(Calendar.DAY_OF_YEAR) &&
                        acctCal.get(Calendar.HOUR_OF_DAY) == activityCal.get(Calendar.HOUR_OF_DAY) &&
                        acctCal.get(Calendar.MINUTE) == activityCal.get(Calendar.MINUTE);
                if (sameHour){
                    System.out.println("FOUND");
                    assert (activity.getPackets() == acct.getPackets() && activity.getSerwer().equals(server1));
                    matchCount++;
                }
            }
        }
        assert (matchCount == minuteCount);
    }

    @Test
    public void youTube(){
	  String userIp = "1.1.1.1";
	  String youtubeIp = "199.223.234.24";
	  int fiveMinuteMillis = 300000;
	  Calendar currentDayCal = Calendar.getInstance();
	  currentDayCal.set(Calendar.DAY_OF_YEAR, currentDayCal.get(Calendar.DAY_OF_YEAR)-1);
	  currentDayCal.set(Calendar.HOUR_OF_DAY,2);
	  currentDayCal.set(Calendar.MINUTE,0);
	  currentDayCal.set(Calendar.SECOND,0);
	  currentDayCal.set(Calendar.MILLISECOND,0);

	  Calendar secondYoutubeCal = Calendar.getInstance();
	  secondYoutubeCal.set(Calendar.DAY_OF_YEAR, secondYoutubeCal.get(Calendar.DAY_OF_YEAR)-1);
	  secondYoutubeCal.set(Calendar.HOUR_OF_DAY,4);
	  secondYoutubeCal.set(Calendar.MINUTE,0);
	  secondYoutubeCal.set(Calendar.SECOND,0);
	  secondYoutubeCal.set(Calendar.MILLISECOND,0);
	  List<Acct> acctList = new LinkedList<>();
	  Map<Calendar,Integer> youtubeMap = new HashMap<>();
	  youtubeMap.put(currentDayCal,3);
	  youtubeMap.put(secondYoutubeCal,5);
	  for(Map.Entry<Calendar,Integer> entry : youtubeMap.entrySet()){
		for (int i = 0; i<entry.getValue();i++){
		  Acct testAcct = AcctManager.createDefaultAcct(youtubeIp,userIp,
				  new Timestamp(entry.getKey().getTime().getTime()+i*fiveMinuteMillis),
				  new Timestamp(entry.getKey().getTime().getTime()+i*fiveMinuteMillis));
		  acctList.add(testAcct);
		  acctRepository.insertAcct(testAcct);
		}
	  }
	  List<YoutubeTimer> youtubeTimerList = acctRepository.getUserYoutubeAcct(userIp);
	  assert (youtubeTimerList.size() == 2);
	  for (Map.Entry<Calendar,Integer> entry : youtubeMap.entrySet()){
	    for (YoutubeTimer youtubeTimer: youtubeTimerList){
	      Calendar youtubeCal = Calendar.getInstance();
	      youtubeCal.setTime(new Date(youtubeTimer.getStartedWatching().getTime()));
		  boolean sameMinute = youtubeCal.get(Calendar.YEAR) == entry.getKey().get(Calendar.YEAR) &&
				  youtubeCal.get(Calendar.DAY_OF_YEAR) == entry.getKey().get(Calendar.DAY_OF_YEAR) &&
				  youtubeCal.get(Calendar.HOUR_OF_DAY) == entry.getKey().get(Calendar.HOUR_OF_DAY) &&
				  youtubeCal.get(Calendar.MINUTE) == entry.getKey().get(Calendar.MINUTE);
		  if (sameMinute){
			System.out.println("FOUND");
			assert(youtubeTimer.getTimeSpent().compareTo(new Date((entry.getValue()-1)*5*60*1000))==0);
		  }
		}
	  }
    }

    @Test
  public void generateTwoWeeksOfData(){
	  String userIp = "1.1.1.1";
	  String server1 = "2.2.2.2";
	  int matchCount = 0;
	  int dayCount = 14;
	  int hourCount = 24;
	  int minuteCount = 60;
	  Calendar today = Calendar.getInstance();
	  Calendar calendar = Calendar.getInstance();
	  for (int i = 0; i < dayCount; i++){
	    calendar.set(Calendar.DAY_OF_YEAR, today.get(Calendar.DAY_OF_YEAR)-i);
	    for (int j = 0; j < hourCount; j++){
		  calendar.set(Calendar.HOUR_OF_DAY, j);
		  for (int n = 0; n < minuteCount; n+=5){
		    calendar.set(Calendar.MINUTE, n);
			Acct recordAcct = AcctManager.createDefaultAcct(server1,userIp,
					new Timestamp(calendar.getTime().getTime()),
					new Timestamp(calendar.getTime().getTime()));
			acctRepository.insertAcct(recordAcct);
			matchCount++;
		  }
		}
	  }
	  System.out.println(matchCount);
	}



}
